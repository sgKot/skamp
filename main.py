import sys
from PySide.QtGui import QApplication

from views.mainwindow import MainWindow

app = None

if __name__ == '__main__':
    app = QApplication(sys.argv)
    frame = MainWindow()
    frame.show()
    app.exec_()
