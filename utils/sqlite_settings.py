import sqlite3 as lite
from datetime import datetime, timedelta

con = lite.connect('settings.db')

def set_vk_settings(vk_key):
    with con:
        cur = con.cursor()
        
        cur.execute("DROP TABLE IF EXISTS VkSettings;")
        cur.execute("CREATE TABLE VkSettings(Id INTEGER PRIMARY KEY, key TEXT, date_expires TEXT);")
        
        date_exp = datetime.now() + timedelta(days=1)
        cur.execute("INSERT INTO VkSettings(key, date_expires) VALUES (?, ?);", (vk_key, date_exp.strftime('%Y-%m-%d %H:%M:%S')))
        
def get_vk_settings():
    with con:
        cur = con.cursor()
        try:
            cur.execute("SELECT key, date_expires FROM VkSettings")
        
            row = cur.fetchone()
            
            date_expires = row[1]
            if datetime.now() <= datetime.strptime(date_expires, '%Y-%m-%d %H:%M:%S'):
                return row[0]
        except lite.Error:
            pass