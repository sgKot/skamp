import re, json
import urllib.request
from PySide.QtCore import QUrl, Signal
from PySide.QtGui import QDialog
from forms.ui_vkdialog import Ui_Dialog
import settings

class VkDialog(QDialog, Ui_Dialog):
    audioListLoaded = Signal(list)
    
    def __init__(self, parent=None):
        super(VkDialog, self).__init__(parent)
        self.setupUi(self)
        
        self.access_token = ''
        self.audio_list = []
        
        self.webView.urlChanged.connect(self.urlChanged)
        
        self.openUrl()
        
    def openUrl(self):
        oauth_url = "https://oauth.vk.com/authorize?client_id=3395240&scope=audio&redirect_uri=https://oauth.vk.com/blank.html&display=popup&response_type=token"
        self.webView.load(QUrl(oauth_url))
        
    def urlChanged(self, url):
        m = re.search('access_token=(.+?)&', url.fragment())
        if m:
            self.access_token = m.group(1)
            self.getAudios(0)
            settings.set_vk_key(self.access_token)
            self.close()
        else:
            self.access_token = ''
        
    def getAudios(self, count=10):
        audio_api = "https://api.vk.com/method/audio.get?count={1}&access_token={0}".format(self.access_token, count)
        resp = urllib.request.urlopen(audio_api)
        res = json.loads(resp.read().decode("utf8"))
        self.audio_list = res['response']
        self.audioListLoaded.emit(self.audio_list)
        
        return self.audio_list