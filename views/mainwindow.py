import os, ntpath
import pybass
from PySide.QtGui import QMainWindow, QFileDialog, QBrush, QColor, QListWidgetItem, QInputDialog
from PySide import QtCore
from plugins import QMySlider
import settings

from forms.ui_mainwindow import Ui_MainWindow

from views.vkdialog import VkDialog


class MainWindow(QMainWindow, Ui_MainWindow):
    def __init__(self, parent=None):
        super(MainWindow, self).__init__(parent)
        self.setupUi(self)

        pybass.BASS_Init(-1, 44100, 0, 0, 0)
        self.horizontalSlider = QMySlider.QMySlider(self.centralwidget)
        self.horizontalSlider.setOrientation(QtCore.Qt.Horizontal)
        self.horizontalSlider.setObjectName("horizontalSlider")

        self.volumeSlider = QMySlider.QMySlider(self.centralwidget)
        self.volumeSlider.setOrientation(QtCore.Qt.Horizontal)
        self.volumeSlider.setObjectName("volumeSlider")
        self.volumeSlider.setMaximum(100)
        self.volumeSlider.setValue(100)

        self.sliderLayout.addWidget(self.horizontalSlider, 1)
        self.sliderLayout.addWidget(self.volumeSlider)

        self.actionOpen_File.triggered.connect(self.openFiles)
        self.listWidget.itemDoubleClicked.connect(self.playFile)
        self.actionPlay.triggered.connect(self.playFile)
        self.actionPause.triggered.connect(self.pauseFile)
        self.actionStop.triggered.connect(self.stopFile)
        self.horizontalSlider.sliderMoved.connect(self.sliderMoved)
        self.volumeSlider.sliderMoved.connect(self.volumeMoved)
        self.actionLoad_Vk_audios.triggered.connect(self.openVkDialog)
        self.actionOpen_Radio.triggered.connect(self.openRadio)
        self.actionNextFile.triggered.connect(self.nextAudio)
        self.actionPrevFile.triggered.connect(self.prevAudio)

        self.handle = 0
        self.selectedItem = None
        self.stopped = False
        self.paused = False
        self.timer = None

    def pathLeaf(self, path):
        head, tail = ntpath.split(path)
        return tail or ntpath.basename(head)

    def getFilledItem(self, data, text, file_type="file"):
        itemWidget = QListWidgetItem()
        itemWidget.setData(QtCore.Qt.UserRole, data)
        itemWidget.setData(QtCore.Qt.UserRole + 1, file_type)
        itemWidget.setText(text)
        return itemWidget

    def closeEvent(self, QCloseEvent):
        pybass.BASS_Free()

    def openFiles(self):
        fileNames = QFileDialog.getOpenFileNames(filter="Music (*.mp1 *.mp2 *.mp3 *.wav *.ogg *.aiff)")

        for fileName in fileNames[0]:
            self.listWidget.addItem(self.getFilledItem(fileName, os.path.splitext(self.pathLeaf(fileName))[0]))

    def playFile(self, fileName=None):
        if not fileName:
            items = self.listWidget.selectedItems()
            if not items:
                return
            fileName = items[0]

        if self.selectedItem:
            self.selectedItem.setForeground(QBrush(QColor(0, 0, 0)))
            self.selectedItem = None
            self.stopped = False

        if isinstance(fileName, QListWidgetItem):
            file_name = fileName.data(QtCore.Qt.UserRole)
            fileName.setForeground(QBrush(QColor(220, 0, 0)))
            self.selectedItem = fileName
        else:
            return

        if self.handle:
            pybass.BASS_ChannelStop(self.handle)

        if not self.paused:
            self.handle = pybass.BASS_StreamCreateFile(False, file_name, 0, 0, pybass.BASS_UNICODE)

        if (not self.paused and not self.handle) or fileName.data(QtCore.Qt.UserRole + 1) == "radio":
            self.handle = pybass.BASS_StreamCreateURL(file_name.encode('ascii'), 0,
                                                      pybass.BASS_STREAM_AUTOFREE or pybass.BASS_UNICODE,
                                                      pybass.DOWNLOADPROC(), 0)
            self.downloadTimer = QtCore.QTimer(self)
            self.downloadTimer.timeout.connect(self.updateDownloadPosition)
            self.downloadTimer.start(500)
            self.horizontalSlider.setMaxDownloadPos(
                pybass.BASS_StreamGetFilePosition(self.handle, pybass.BASS_FILEPOS_END))

        pybass.BASS_ChannelSetAttribute(self.handle, pybass.BASS_ATTRIB_VOL, self.volumeSlider.value() / 100)

        pybass.BASS_ChannelPlay(self.handle, False)

        self.paused = False

        self.toolBar.removeAction(self.actionPlay)
        self.toolBar.insertAction(self.actionStop, self.actionPause)

        lenBytes = pybass.BASS_ChannelGetLength(self.handle, pybass.BASS_POS_BYTE);
        self.horizontalSlider.setMaximum(lenBytes)

        if not self.timer:
            self.timer = QtCore.QTimer(self)
            self.timer.timeout.connect(self.updatePosition)
        else:
            self.timer.stop()
        self.horizontalSlider.setValue(0)
        self.timer.start(500)

    def pauseFile(self):
        if self.handle:
            self.paused = True
            pybass.BASS_ChannelPause(self.handle)
            self.toolBar.removeAction(self.actionPause)
            self.toolBar.insertAction(self.actionStop, self.actionPlay)

    def stopFile(self):
        if self.handle:
            pybass.BASS_ChannelStop(self.handle)
            self.horizontalSlider.setValue(0)
            self.stopped = True
            self.timer.stop()
            if not self.paused:
                self.toolBar.removeAction(self.actionPause)
                self.toolBar.insertAction(self.actionStop, self.actionPlay)

    def updatePosition(self):
        if pybass.BASS_ChannelIsActive(self.handle) == pybass.BASS_ACTIVE_STOPPED:
            self.nextAudio()
            return

        lenPos = pybass.BASS_ChannelGetPosition(self.handle, pybass.BASS_POS_BYTE)
        self.horizontalSlider.setValue(lenPos)

    def updateDownloadPosition(self):
        if pybass.BASS_ChannelIsActive(self.handle) == pybass.BASS_ACTIVE_STOPPED:
            return

        lenPos = pybass.BASS_StreamGetFilePosition(self.handle, pybass.BASS_FILEPOS_DOWNLOAD)
        self.horizontalSlider.setDownloadPos(lenPos)

    def sliderMoved(self, pos):
        pybass.BASS_ChannelSetPosition(self.handle, pos, pybass.BASS_POS_BYTE)

    def volumeMoved(self, pos):
        if self.handle:
            pybass.BASS_ChannelSetAttribute(self.handle, pybass.BASS_ATTRIB_VOL, pos / 100)

    def openVkDialog(self):
        dialog = VkDialog(self)
        dialog.audioListLoaded.connect(self.addAudioFromVk)
        key = settings.get_vk_key()
        if not key:
            dialog.show()
        else:
            dialog.access_token = key
            dialog.getAudios(0)

    def addAudioFromVk(self, audioList):
        for audio in audioList:
            self.listWidget.addItem(
                self.getFilledItem(audio['url'], "{0} - {1}".format(audio['artist'], audio['title'])))

    def openRadio(self):
        radio_url = QInputDialog.getText(self, "Enter radio url", "Url:")[0]
        self.listWidget.addItem(self.getFilledItem(radio_url, radio_url, "radio"))

    def nextAudio(self):
        if self.selectedItem:
            index = self.selectedItem.listWidget().row(self.selectedItem)
            if index == self.listWidget.count():
                index -= 1
            item = self.listWidget.item(index + 1)
            self.playFile(item)

    def prevAudio(self):
        if self.selectedItem:
            index = self.selectedItem.listWidget().row(self.selectedItem)
            if index == 0:
                index = 1
            item = self.listWidget.item(index - 1)
            self.playFile(item)