from utils import sqlite_settings

def get_vk_key():
    return sqlite_settings.get_vk_settings()

def set_vk_key(key):
    sqlite_settings.set_vk_settings(key)