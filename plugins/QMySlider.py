from PySide.QtCore import Qt, Signal
from PySide.QtGui import QSlider, QStyle, QStyleOptionSlider

class QMySlider(QSlider):
    downloadPosChanged = Signal(int)
    
    def __init__(self, parent=None):
        super(QMySlider, self).__init__()
        self.pressedControl = QStyle.SC_None
        self._downloadPos = 0;
        self._maxDownloadPos = 100;
        
    def downloadPos(self):
        return self._downloadPos
    
    def setDownloadPos(self, pos):
        self._downloadPos = pos
        self.downloadPosChanged.emit(self.downloadPos)
        
    def maxDownloadPos(self):
        return self._maxDownloadPos
    
    def setMaxDownloadPos(self, pos):
        self._maxDownloadPos = pos
        
    def pick(self, pt):
        return pt.x() if self.orientation() == Qt.Horizontal else pt.y()
    
    def pixelPosToRangeValue(self, pos):
        opt = QStyleOptionSlider()
        self.initStyleOption(opt)
        
        gr = self.style().subControlRect(QStyle.CC_Slider, opt, QStyle.SC_SliderGroove, self)
        sr = self.style().subControlRect(QStyle.CC_Slider, opt, QStyle.SC_SliderHandle, self)

        if self.orientation() == Qt.Horizontal:
            sliderLength = sr.width()
            sliderMin = gr.x()
            sliderMax = gr.right() - sliderLength + 1;
        else:
            sliderLength = sr.height()
            sliderMin = gr.y()
            sliderMax = gr.bottom() - sliderLength + 1;
        
        return QStyle.sliderValueFromPosition(self.minimum(), self.maximum(), pos - sliderMin, sliderMax - sliderMin)
    
    def rangeValueToPixelPos(self, val, minimum = -1, maximum = -1):
        opt = QStyleOptionSlider()
        self.initStyleOption(opt)
        opt.subControls = QStyle.SC_All;
        available = opt.rect.width() - self.style().pixelMetric(QStyle.PM_SliderLength, opt, self);
        if minimum == -1: minimum = opt.minimum
        if maximum == -1: maximum = opt.maximum
        return QStyle.sliderPositionFromValue(minimum, maximum, val, available);

    def mousePressEvent(self, ev):
        ev.accept()
        if (ev.button() & self.style().styleHint(QStyle.SH_Slider_AbsoluteSetButtons, None, None, None)) == ev.button():
            opt = QStyleOptionSlider()
            self.initStyleOption(opt)
            sliderRect = self.style().subControlRect(QStyle.CC_Slider, opt, QStyle.SC_SliderHandle, self)
            center = sliderRect.center() - sliderRect.topLeft()
            self.setSliderPosition(self.pixelPosToRangeValue(self.pick(ev.pos() - center)))
            self.triggerAction(self.SliderMove)
            self.setRepeatAction(self.SliderNoAction)
            self.pressedControl = QStyle.SC_SliderHandle
            self.update()
        elif (ev.button() & self.style().styleHint(QStyle.SH_Slider_PageSetButtons, None, None, None)) == ev.button():
            opt = QStyleOptionSlider()
            self.initStyleOption(opt)
            self.pressedControl = self.style().hitTestComplexControl(QStyle.CC_Slider, opt, ev.pos(), self)
            
            if self.pressedControl == QStyle.SC_SliderGroove:
                sliderRect = self.style().subControlRect(QStyle.CC_Slider, opt, QStyle.SC_SliderHandle, self)
                pressValue = self.pixelPosToRangeValue(self.pick(ev.pos() - sliderRect.center() + sliderRect.topLeft()))
                
                self.setSliderPosition(pressValue)
                self.sliderMoved.emit(pressValue)
        else:
            ev.ignore()
            return
        
        if self.pressedControl == QStyle.SC_SliderHandle:
            opt = QStyleOptionSlider()
            self.initStyleOption(opt)
            self.setRepeatAction(self.SliderNoAction)
            sr = self.style().subControlRect(QStyle.CC_Slider, opt, QStyle.SC_SliderHandle, self)
            self.clickOffset = self.pick(ev.pos() - sr.topLeft())
            self.update(sr)
            self.setSliderDown(True)
    
    def mouseMoveEvent(self, ev):
        if self.pressedControl != QStyle.SC_SliderHandle:
            ev.ignore()
            return
        
        ev.accept()
        newPosition = self.pixelPosToRangeValue(self.pick(ev.pos()) - self.clickOffset)
        opt = QStyleOptionSlider()
        self.initStyleOption(opt)
        self.setSliderPosition(newPosition)
    
    def mouseReleaseEvent(self, ev):
        if self.pressedControl == QStyle.SC_None or ev.buttons():
            ev.ignore()
            return
        
        ev.accept()
        oldPressed = QStyle.SubControl(self.pressedControl)
        self.pressedControl = QStyle.SC_None
        self.setRepeatAction(self.SliderNoAction)
        if oldPressed == QStyle.SC_SliderHandle:
            self.setSliderDown(False)
        opt = QStyleOptionSlider()
        self.initStyleOption(opt)
        opt.subControls = oldPressed
        self.update(self.style().subControlRect(QStyle.CC_Slider, opt, oldPressed, self))
        
